package com.example.demo.controller;

import com.example.demo.entity.Person;
import com.example.demo.repository.PersonRepository;
import java.util.List;
import java.util.UUID;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/person")
public class PersonController {

  private PersonRepository personRepository;

  public PersonController(PersonRepository personRepository) {
    this.personRepository = personRepository;
  }

  @PostMapping
  public Person save(@RequestBody Person person){
    Person personSaved = personRepository.save(person);
    return personSaved;
  }

  @GetMapping
  public List<Person> getAll(){
    List<Person> people = personRepository.findAll();
    return people;
  }

  @PutMapping("/{id}")
  public Person update(@PathVariable UUID id, @RequestBody Person person){
    Person personFound = personRepository.findById(id).get();
    personFound.setName(person.getName());
    personFound.setLastName(person.getLastName());
    personFound.setAge(person.getAge());
    return personRepository.save(personFound);
  }

  @DeleteMapping("/{id}")
  public void delete(@PathVariable UUID id){
    personRepository.deleteById(id);
  }
}
